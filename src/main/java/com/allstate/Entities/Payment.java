package com.allstate.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Document
public class Payment {
    //region variables
    @Id
    private int id;
    private LocalDate paymentDate;
    private String type;
    private double amount;
    private int custId;
    //endregion variables

    //region getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type.toLowerCase();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }
    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    //endregion getters and setters

    @Override
    public String toString() {
        return "ID: " + this.id + "\nPayment Date: " + this.paymentDate + "\nType: "
                + this.type + "\nAmount: " + this.amount + "\nCustomer ID: " + this.custId;
    }

    //region constructors
    public Payment(int id, LocalDate paymentDate, String type, double amount, int custId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type.toLowerCase();
        this.amount = amount;
        this.custId = custId;

        System.out.println(this.toString());
    }

    public Payment() {

    }

//endregion constructors
}
