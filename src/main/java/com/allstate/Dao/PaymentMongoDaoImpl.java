package com.allstate.Dao;

import com.allstate.Entities.Payment;
import jdk.jfr.Registered;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class PaymentMongoDaoImpl implements IPaymentDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowcount() {
        List<Payment> paymentList = mongoTemplate.findAll(Payment.class);
        return paymentList.size();
    }

    @Override
    public Payment findByID(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query,Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList = new ArrayList<>();

        for (Payment payment : mongoTemplate.find(query,Payment.class)) {
        paymentList.add(payment);
        }
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        return this.rowcount();
    }

    @Override
    public Collection<Payment> getAllPayments() {
        return mongoTemplate.findAll(Payment.class);
    }
}
