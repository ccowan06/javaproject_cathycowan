package com.allstate.services;

import com.allstate.Entities.Payment;

import java.util.Collection;
import java.util.List;

public interface IPaymentService {
    int rowcount();
    Payment findByID(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    Collection<Payment> getAllPayments();

}
