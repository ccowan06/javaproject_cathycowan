package com.allstate.services;

import com.allstate.Dao.IPaymentDao;
import com.allstate.Entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PaymentService implements IPaymentService {
    @Autowired
    private IPaymentDao dao;

    @Override
    public int rowcount() {
        return dao.rowcount();
    }

    @Override
    public Payment findByID(int id) {

        if (id > 0) {
            return dao.findByID(id);
        }
        else {
            return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        if (type != null) {
            return dao.findByType(type);
        } else {
            return null;
        }
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }

    @Override
    public Collection<Payment> getAllPayments() {
        return dao.getAllPayments();
    }
}
