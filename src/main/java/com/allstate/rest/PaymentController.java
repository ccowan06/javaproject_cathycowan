package com.allstate.rest;

import com.allstate.Entities.Payment;
import com.allstate.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/payments")
@CrossOrigin(value="*")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
    Logger logger = Logger.getLogger(PaymentController.class.getName());
    //region gets

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus()
    {
        logger.info("Status method\n");
        return "Rest Api is running";
    }

    //not required all below but wanted to check what was in the db over the API
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Collection<Payment> getAllPayments() {
        logger.info("Get all payments\n");
        return paymentService.getAllPayments();
    }

    @RequestMapping(value = "/numberofrows", method = RequestMethod.GET)
    public int rowcount() {
        logger.info("number of rows\n");
        return paymentService.rowcount();
    }

    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findbyIDHandling404(@PathVariable("id") int id) {
        Payment payment = paymentService.findByID(id);
        logger.info("Find by ID\n");

        if (payment == null){

            logger.info("Find by ID: " + id + " not found\n");
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    public ResponseEntity<List> findbyTypeHandling404(@PathVariable("type") String type) {
        List<Payment> paymentList= paymentService.findByType(type);
        logger.info("Find by Type\n");

        if (paymentList.size() == 0){
            logger.info("Find by Type: " + type + " not found\n");
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(paymentList,HttpStatus.OK);
        }
    }

    //endregion gets

    //region post
    @RequestMapping(method = RequestMethod.POST)
    public int addPayment(@RequestBody Payment payment) {
        return paymentService.save(payment);
    }

    //endregion post
}
