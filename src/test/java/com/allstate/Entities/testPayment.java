package com.allstate.Entities;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import static org.springframework.test.util.AssertionErrors.assertEquals;

public class testPayment {

    @Test
    public void testPaymentConstructor(){

        Payment payment = new Payment(10, LocalDate.now(), "type",10.99,22);
        assertEquals("ID", 10, payment.getId());
        assertEquals("Payment Date", LocalDate.now(), payment.getPaymentDate());
        assertEquals("Type", "type", payment.getType());
        assertEquals("Amount", 10.99, payment.getAmount());
        assertEquals("Customer ID", 22, payment.getCustId());
    }

}
