package com.allstate.Services;

import com.allstate.Entities.Payment;
import com.allstate.services.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.util.AssertionErrors;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
public class testPaymentService {
    @Autowired
    private PaymentService service;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void cleanUp() {

            for (String collectionName : mongoTemplate.getCollectionNames()) {
                if (!collectionName.startsWith("system")) {
                    mongoTemplate.dropCollection(collectionName);
                }
            }
        }
//region service layer int tests
    @Test
    public void addPaymentThroughServiceLayer(){
        Payment payment= new Payment(11, LocalDate.now(), "Visa",10.99,20);
        assertEquals(1,service.save(payment));
    }

    @Test
    public void findbyIDThroughSL()
    {
        Payment payment= new Payment(22, LocalDate.now(), "Visa",10.99,20);
        service.save(payment);

        assertNotNull("check found", service.findByID(22));
    }

    @Test
    public void testfindByType(){
        Payment[] payments = new Payment[]{
                new Payment(1, LocalDate.now(), "Visa",10.99,20),
                new Payment(2, LocalDate.now(),"Mastercard", 110.21,21),
                new Payment(3, LocalDate.now(), "Mastercard", 120.99, 22),
                new Payment(4, LocalDate.now(), "Paypal",12.99,24),
                new Payment(5, LocalDate.now(), "Paypal",12.99,24),
                new Payment(6, LocalDate.now(), "Paypal",12.99,24)
        };

        List<Payment> paymentListOrig = new ArrayList<>();

        for (Payment payment : payments) {
            service.save(payment);
            if (payment.getType().toLowerCase().equals("paypal")){
                paymentListOrig.add(payment);
            }
        }

        List<Payment> paymentList = service.findByType("Paypal");

        System.out.println("\n -------- Original list -------\n" + paymentListOrig);

        System.out.println("\n -------- db list -------\n" + paymentList);
        AssertionErrors.assertEquals("Check list same size by find by type Paypal", paymentListOrig.size(), paymentList.size());
    }

    @Test
    public void testRowCountThroughService(){
        Payment[] payments = new Payment[]{
                new Payment(1, LocalDate.now(), "Visa",10.99,20),
                new Payment(2, LocalDate.now(),"Mastercard", 110.21,21),
                new Payment(3, LocalDate.now(), "Mastercard", 120.99, 22),
                new Payment(4, LocalDate.now(), "Paypal",12.99,24),
                new Payment(5, LocalDate.now(), "Paypal",12.99,24),
                new Payment(6, LocalDate.now(), "Paypal",12.99,24)
        };

        for (Payment payment : payments) {
            service.save(payment);
        }

        AssertionErrors.assertEquals("Number of rows should equals 6", 6, service.rowcount());
    }
//endregion
}
