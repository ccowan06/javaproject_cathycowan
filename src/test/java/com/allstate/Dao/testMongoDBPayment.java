package com.allstate.Dao;


import com.allstate.Entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.swing.text.ParagraphView;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@SpringBootTest
public class testMongoDBPayment  {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IPaymentDao dao;

    @BeforeEach
    public void cleanUp() {
        if (mongoTemplate.getCollectionNames() != null) {
            for (String collectionName : mongoTemplate.getCollectionNames()) {
                if (!collectionName.startsWith("system")) {
                    mongoTemplate.dropCollection(collectionName);
                }
            }
        }
        }

    @Test
    public void testCanInsertSuccessfullyUsingMongoTemplate() {

        Payment[] payments = new Payment[]{
                new Payment(1, LocalDate.now(), "Visa",10.99,20),
                new Payment(2, LocalDate.now(),"Mastercard", 110.21,21),
                new Payment(3, LocalDate.now(), "Revolut", 120.99, 22)
        };

        for (Payment payment : payments) {
            mongoTemplate.insert(payment);
        }
        Collection<Payment> lstPayments = mongoTemplate.findAll(Payment.class);
        lstPayments.forEach(payment -> System.out.println(payment.getCustId()));
        assertEquals("number of rows",3,lstPayments.size());
    }

    //region integration tests for the DAO class
    @Test
    public void testfindByType(){
        Payment[] payments = new Payment[]{
                new Payment(1, LocalDate.now(), "Visa",10.99,20),
                new Payment(2, LocalDate.now(),"Mastercard", 110.21,21),
                new Payment(3, LocalDate.now(), "Mastercard", 120.99, 22),
                new Payment(4, LocalDate.now(), "Paypal",12.99,24),
                new Payment(5, LocalDate.now(), "Paypal",12.99,24),
                new Payment(6, LocalDate.now(), "Paypal",12.99,24)
        };

        List<Payment> paymentListOrig = new ArrayList<>();

        for (Payment payment : payments) {
            dao.save(payment);
            if (payment.getType().toLowerCase().equals("paypal")){
                paymentListOrig.add(payment);
            }
        }

        List<Payment> paymentList = dao.findByType("paypal");

        System.out.println("\n -------- Original list -------\n" + paymentListOrig);

        System.out.println("\n -------- db list -------\n" + paymentList);
        assertEquals("Check list same size by find by type Paypal", paymentListOrig.size(), paymentList.size());
    }

    @Test
    public void testCanSaveAndFindBId(){

        Payment payment= new Payment(4, LocalDate.now(), "Paypal",12.99,24);
        System.out.println("Number of rows in Database is : " + dao.save(payment));

        payment= dao.findByID(4);

        assertNotNull("Check if payment is found" , payment);
    }

    @Test
    public void testRowCount(){
        Payment[] payments = new Payment[]{
                new Payment(1, LocalDate.now(), "Visa",10.99,20),
                new Payment(2, LocalDate.now(),"Mastercard", 110.21,21),
                new Payment(3, LocalDate.now(), "Mastercard", 120.99, 22),
                new Payment(4, LocalDate.now(), "Paypal",12.99,24),
                new Payment(5, LocalDate.now(), "Paypal",12.99,24),
                new Payment(6, LocalDate.now(), "Paypal",12.99,24)
        };

        for (Payment payment : payments) {
            dao.save(payment);
        }

        assertEquals("Number of rows should equals 6", 6, dao.rowcount());
    }
//endregion
}
